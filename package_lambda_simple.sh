#!/bin/bash -e
LAMBDA_DIR="$1";

cd $LAMBDA_DIR
# for each subdirectory of LAMBDA_DIR, zip the contents
for D in `find . -mindepth 1 -type d`
do
  cd $D
  zip $D.zip *
  mv $D.zip ../
  cd ../
  echo "created $D.zip"
done
